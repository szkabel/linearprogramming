classdef classicalPivot < PivotSelection
    %classicalPivot selection
        
    
    methods
        function [obj] = classicalPivot(optM)
            obj.optimizationMethod = optM;
        end
        
        function [status,i,j] = selectPivot(obj,A)
            status = 0; i = 0; j = 0;
            if obj.optimizationMethod == 'max'
                [~,selectedColumn] = max(A(end,1:end-1));
                if A(end,selectedColumn)<=0
                    status = -1; %optimum                    
                    return;
                end
            else
                [~,selectedColumn] = min(A(end,1:end-1));
                if A(end,selectedColumn)>=0
                    status = -1; %optimum
                    return;
                end
            end            
            lineIndex = find(A(1:end-1,selectedColumn)>0);
            if isempty(lineIndex)
                status = -2; % non limited task
            end
            [~,selectedRow] = min(A(lineIndex,end)./A(lineIndex,selectedColumn));
            selectedRow = lineIndex(selectedRow);
            i = selectedRow;
            j = selectedColumn;
        end
    end
    
end


