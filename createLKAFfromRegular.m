function [ A,base ] = createLKAFfromRegular( A,b,c)

base = size(A,2)+1:size(A,2)+size(A,1);
A = [A eye(size(A,1)) b; [c zeros(1,size(A,1)+1)]];

end

