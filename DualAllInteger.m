function [ x ] = DualAllInteger( A,b,c )
%Ax <= b and c*x is minimized. A,b,c are integers.

printMode = 'simplex';

%init step
[A,base] = createLKAFfromRegular(A,b,c);

nonbase = setdiff(1:size(A,2)-1,base);

while ~all(A(1:end-1,end)>=0)
    printState(A,printMode,base,nonbase)
    K = find(A(1:end-1,end)<0,1,'first');
    lambda = abs(min(A(K,1:end-1)));
    newA = zeros(size(A,1)+1,size(A,2)+1);
    newA(1:end-2,1:end-2) = A(1:end-1,1:end-1); 
    newA(end,1:end-2) = A(end,1:end-1);
    newA(1:end-2,end) = A(1:end-1,end);
    newA(end,end) = A(end,end);
    
    base(end+1) = size(newA,2)-1;
    newA(end-1,end-1) = 1; % extend the base
    newA(end-1,setdiff([1:size(newA,2)-1],base)) = floor(A(K,setdiff([1:size(newA,2)-1],base))/lambda);
    newA(end-1,end) = floor(A(K,end)/lambda);
    A = newA;
    printState(A,printMode,base,nonbase);
        
    [status,i,j] = dualPivot(A,size(A,1)-1);
    if status == -1
        x = zeros(1,size(A,2)-1);
        fprintf('Optimum. The best value is: %f\n',-A(end,end));
        disp('The optimal solution is:');
        x(base) = A(1:end-1,end);    
        disp(x);
        return;
    end
    if status == -3
        fprintf('The task is not solvable');
        return;
    end
    M = EliminaciosMatrix(A,i,j);
    A = M*A;
    %pivot step on the variables
    nonbase(nonbase == j) = base(i);
    base(i) = j;
end

printState(A,printMode,base,nonbase)
x = zeros(1,size(A,2)-1);
fprintf('Optimum. The best value is: %f\n',-A(end,end));
disp('The optimal solution is:');
x(base) = A(1:end-1,end);    
disp(x);



end

