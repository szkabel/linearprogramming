function [ x ] = simplexAlg( A, pivotSelection,base)
%A is coming already in the big form

[status,r,c] = pivotSelection.selectPivot(A);

while status==0    
    disp(A);
    fprintf('*******************\n');
    M = EliminaciosMatrix(A,r,c);
    base(r) = c;
    A = M*A;    
    [status,r,c] = pivotSelection.selectPivot(A);    
end

disp(A);

if status == -1
    x = zeros(1,size(A,2)-1);
    fprintf('Optimum. The best value is: %f\n',-A(end,end));
    disp('The optimal solution is:');
    x(base) = A(1:end-1,end);    
    disp(x);
end

if status == -2
    fprintf('The task is not limited');
end

end

