A DUAL ALL INTEGER algoritmus megvalósítása MATLABban.

Azaz, Ey + Ax = b alakú feltételrendszerben a c*x -> min célfüggvény optimumát keressük.

Kikötések: A,b,c mind csupa egészek. c>0.

A program 'main' függvénye a DualAllInteger függvény, amely az A,b,c paramétereket várja (tehát az egységmátrixot NEM kell megadni - a háziban ezek az y-hoz tartozó számok).

pl.: egy feladat
y1 y2 y3 x1 x2 x3
 1  0  0 -7  3 -2 | -3
 0  1  0 -4 -2  1 |  2
 0  0  1  8  5 -7 |  4
 0  0  0 11 13  8 |
 
 Az ehhez tartozó paraméterek MATLAB-ban:
 
 >> A = [-7 3 -2; -4 -2 1; 8 5 -7];
 >> b = [-3; 2; 4];
 >> c = [11 13 8];
 Majd,
 >> DualAllInteger(A,b,c);
 
 Figyeljetek a vektorok dimenzióira (a b legyen oszlopvektor ;-vel elválasztva, a c legyen sorvektor)
 
 A jelenlegi beállítások szerint a megfelelő szimplex táblákat köpi ki a program magából. (a DualAllInteger fgv. 4. sorában át lehet ezt állítani 'matrix'-ra is)
 
 A változók azonosítása: az y-ok az eredeti x-en fölüli indexek.
 i.e. a fönti példában x1,x2,x3 után y1 = x4, y2 = x5, y3 = x6 stb. A változóknak csak az indexe van kiírva.
 
 A base változók amiket bal oldalra kell írni
 A nonbase változók amiket a fejlécbe kell írni.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.