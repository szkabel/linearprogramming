function M = EliminaciosMatrix(A,i,j)
    M = eye(size(A,1));
    
    M(:,i) = -A(:,j)/A(i,j);
    M(i,i) = 1/A(i,j);
end