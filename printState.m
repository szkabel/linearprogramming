function  printState( A, type, base, nonbase )
%type can be: 'matrix' or 'simplex'
% the latter 2 is optional in matrix mode but compulsary in simplex mode

fprintf('***********************************\n')
fprintf('The form of the task is: \n')

precision = 0;
lengthOfPrint = 5;
switch type
    case 'matrix'                
        for i=1:size(A,1)-1
            for j=1:size(A,2)-1
                fprintf(['% ' num2str(lengthOfPrint)  '.' num2str(precision) 'f '],A(i,j));
            end
            fprintf([' = % ' num2str(lengthOfPrint)  '.' num2str(precision) 'f\n'],A(i,end));
        end
        fprintf('-------------------------------------\n');
        for j=1:size(A,2)-1
            fprintf(['% ' num2str(lengthOfPrint)  '.' num2str(precision) 'f '],A(end,j));
        end
        fprintf([' = % ' num2str(lengthOfPrint)  '.' num2str(precision) 'f\n'],A(end,end));
    case 'simplex'
       for i=1:size(A,1)-1
           for j=nonbase
               fprintf(['% ' num2str(lengthOfPrint)  '.' num2str(precision) 'f '],A(i,j));
           end
           fprintf([' = % ' num2str(lengthOfPrint)  '.' num2str(precision) 'f\n'],A(i,end));
       end
       fprintf('-------------------------------------\n');
       for j=nonbase
            fprintf(['% ' num2str(lengthOfPrint)  '.' num2str(precision) 'f '],A(end,j));
        end
        fprintf([' = % ' num2str(lengthOfPrint)  '.' num2str(precision) 'f\n'],A(end,end));
end

if nargin>2
    disp('..........................');
    fprintf('The base variables:\n');
    disp(base);
    if nargin > 3
        fprintf('The nonbase variables:\n');
        disp(nonbase);
    end
end

end