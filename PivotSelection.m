classdef (Abstract) PivotSelection
    %You should provide a constructor for this class, that says if we are
    %maximizing or minimizing.
    
    properties
        %String storing: min or max
        optimizationMethod;
    end
    
    methods (Abstract)
        %status describes if we are in optimum (-1), if the task is non
        %limited (-2) or if we can take a next pivot step (0)
        [status,i,j] = selectPivot(A);
    end
    
end

