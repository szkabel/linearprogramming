function [ status, k,j ] = dualPivot( A, row)
%Pivot selection for the dual simplex algorithm
%   if the row is given, then we are forced to select from that row
%   status -1 optimum, -3 not solvable
%   for minimiaztion
status = 0; k = 0; j = 0;

if nargin>1
    k = row;
    if A(k,end)>0
        error('Wrongly selected row!');        
    end
    bk = A(k,end);
else
    [bk,k] = min(A(1:end-1,end));
    k = k(1); % select the one with the smallest index
end

if bk>=0
    status = -1; %optimum
    return;
end

logIndex = find(A(k,1:end-1)<0);
if isempty(logIndex)
    status = -3; %not solvable
    return;
end

[~,j] = min(A(end,logIndex)./-A(k,logIndex)); %select pivot
j = logIndex(j); % go back to the original matrix indexing


end

